import './App.css';

import React, { useState } from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import LeftIcon from '@mui/icons-material/ChevronLeft';
import LaptopIcon from '@mui/icons-material/Laptop';

import WorkspacesRequestPage from './WorkspacesRequest';
import {  BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

function App() {

  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };  

  return (
    <div className="App">
            <Router>

      <AppBar position="static">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerOpen}
          >
          <MenuIcon />
          </IconButton>   
          <Typography variant="h6" className="test">
            My App
          </Typography>
        </Toolbar>
      </AppBar>   

      <Drawer
        variant="persistent"
        anchor="left"
        open={open}
      >
        <div/>
        <List>

          <ListItem button onClick={handleDrawerClose}>
            <ListItemIcon>
              <LeftIcon />
            </ListItemIcon>
          </ListItem>

          <Divider/>

          <ListItem>
            <ListItemText primary="Workspaces" />
          </ListItem>

          <Link to="/workspaces-request">
          <ListItem button>
            <ListItemIcon>
              <LaptopIcon />
            </ListItemIcon>
            <ListItemText primary="Create Workspace" />
          </ListItem>
          </Link>

          <Divider/>

          <ListItem>
            <ListItemText primary="Datadog" />
          </ListItem>   

          <ListItem button>
            <ListItemIcon>
              <LaptopIcon />
            </ListItemIcon>
            <ListItemText primary="Datadog Access" />
          </ListItem>                 

        </List>
      </Drawer>

        <Routes>
        <Route path="/" element={<WorkspacesRequestPage/>}></Route>
          <Route path="/workspaces-request" element={<WorkspacesRequestPage/>}></Route>
        </Routes>
      </Router>      

    </div>
  );
}

export default App;

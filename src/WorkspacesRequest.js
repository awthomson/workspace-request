import React, { useState } from 'react';
import { TextField, Button, Paper, Snackbar, Alert } from '@mui/material';

const emailRegEx = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i;
const nameRegEx = /^[A-Z][a-z]+\s[A-Za-z'-\s]+[a-z]+$/i;
const freeTextRegEx = /^[A-Za-z0-9\s-_]+$/i;

const WorkspacesRequestPage = () => {

  const [formData, setFormData] = useState({
    fullName: '',
    fullNameError: false,
    fullNameErrorMsg: ' ',
    email: '',
    emailError: false,
    emailErrorMsg: ' ',
    accountName: '',
    accountNameError: false,
    accountNameErrorMsg: ' ',    
    requester: '',
    requesterError: false,
    requesterErrorMsg: ' ',
    project: '',
    projectError: false,
    projectErrorMsg: ' '
  });

  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
      email: "Test"+value,
    }));
  };

  const handleClear = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      fullName: '',
      email: '',
      requester: '',
      project: '',
    }));
  }

  const handleSubmit = (e) => {

    e.preventDefault();

    var errorFound = validateFullName();
    errorFound = errorFound | validateEmail();
    errorFound = errorFound | validateRequester();
    errorFound = errorFound | validateProject();

    if (!errorFound) {
      setSnackbarOpen(true);
    }

  };

  const validateFullName = () => {
    var errorFound = false;
    if (formData.fullName === '') {
      setFormData((prevState) => ({
        ...prevState,
        fullNameError: true,
        fullNameErrorMsg: "This field is required.",
      }));      
      errorFound = true;
    } else if (nameRegEx.test(formData.fullName)) {
      setFormData((prevState) => ({
        ...prevState,
        fullNameError: false,
        fullNameErrorMsg: '',
      }));
    } else {
      setFormData((prevState) => ({
        ...prevState,
        fullNameError: true,
        fullNameErrorMsg: "Invalid value.",
      }));
      errorFound = true;
    }
    return errorFound;
  }

  const validateEmail = () => {
    var errorFound = false;
    if (formData.email === '') {
      setFormData((prevState) => ({
        ...prevState,
        emailError: true,
        emailErrorMsg: "This field is required.",
      }));  
      errorFound = true;
    } else if (emailRegEx.test(formData.email)) {
        setFormData((prevState) => ({
          ...prevState,
          emailError: false,
          emailErrorMsg: ''
        }));
    } else {
      setFormData((prevState) => ({
        ...prevState,
        emailError: true,
        emailErrorMsg: 'Invalid value',
      }));
      errorFound = true;
    }
    return errorFound;
  }

  const validateRequester = () => {
    var errorFound = false;
    if (formData.requester === '') {
      setFormData((prevState) => ({
        ...prevState,
        requesterError: true,
        requesterErrorMsg: "This field is required.",
      }));  
      errorFound = true;
    } else if (nameRegEx.test(formData.requester)) {
        setFormData((prevState) => ({
          ...prevState,
          requesterError: false,
          requesterErrorMsg: ''
        }));
    } else {
      setFormData((prevState) => ({
        ...prevState,
        requesterError: true,
        requesterErrorMsg: 'Invalid value',
      }));
      errorFound = true;
    }  
    return errorFound;
  }

  const validateProject = () => {
    var errorFound = false;
    if (formData.project === '') {
      setFormData((prevState) => ({
        ...prevState,
        projectError: true,
        projectErrorMsg: "This field is required.",
      }));  
      errorFound = true;
    } else if (freeTextRegEx.test(formData.project)) {
        setFormData((prevState) => ({
          ...prevState,
          projectError: false,
          projectErrorMsg: ''
        }));
    } else {
      setFormData((prevState) => ({
        ...prevState,
        projectError: true,
        projectErrorMsg: 'Invalid value',
      }));
      errorFound = true;
    }    
    return errorFound;
  }

  const paperStyle = {
      padding: '2rem',
      width: '50%',
      marginLeft: '25%',
      marginTop: '4rem'
  };

  const handleSnackbarClose = (event, reason) => {
    setSnackbarOpen(false);
  };  

  return (
    <div>
    <Paper style={paperStyle}>
      <h1>Request AWS Workspace Instance</h1>
    <form onSubmit={handleSubmit}>
      <TextField
        name="fullName"
        label="Users Full Name"
        value={formData.fullName}
        onChange={handleChange}
        onBlur={validateFullName}
        fullWidth
        error={formData.fullNameError}
        helperText={formData.fullNameErrorMsg}        
        margin="normal"
      />
      <TextField
        name="email"
        label="Users Email Address"
        value={formData.email}
        variant="filled"
        InputProps={{
          readOnly: true,
        }}
        fullWidth
        margin="normal"
      />  
      <TextField
        name="accountname"
        label="Users Account Name"
        value={formData.accountName}
        variant="filled"
        InputProps={{
          readOnly: true,
        }}
        fullWidth
        margin="normal"
      />            
      <TextField
        name="requester"
        label="Requested By"
        value={formData.requester}
        onChange={handleChange}
        onBlur={validateRequester}   
        // variant="filled"     
        fullWidth
        error={formData.requesterError}
        helperText={formData.requesterErrorMsg}
        margin="normal"
      />      
      <TextField
        name="project"
        label="Project"
        value={formData.project}
        onChange={handleChange}
        onBlur={validateProject}
        // variant="filled"
        fullWidth
        error={formData.projectError}
        helperText={formData.projectErrorMsg}
        margin="normal"
      />      
      <Button type="submit" variant="contained" color="primary">
        Submit
      </Button>
      <Button variant="contained" color="secondary" onClick={handleClear}>
        Clear
      </Button>      
    </form>
    </Paper>
    <Snackbar open={snackbarOpen} autoHideDuration={10000} onClose={handleSnackbarClose}>
      <Alert severity="success">
        Request successfully submitted
      </Alert>      
    </Snackbar>
    </div>
  );
}

export default WorkspacesRequestPage;

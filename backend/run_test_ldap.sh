#!/bin/bash
docker kill "/openldap"
docker run --detach --rm --name openldap \
  --env LDAP_ROOT="dc=example,dc=com" \
  --env LDAP_ADMIN_USERNAME=admin \
  --env LDAP_ADMIN_PASSWORD=adminpassword \
  --env LDAP_USERS=user1,user2 \
  --env LDAP_PASSWORDS=password1,password2 \
  -p 1389:1389 \
  bitnami/openldap:latest

import ldap3
from flask import Flask, request

app = Flask(__name__)

ldap_host = "localhost:1389"
binddn    = "cn=admin,dc=example,dc=com"
bindpw    = "adminpassword"
basedn    = "dc=example,dc=com"

server = ""
conn = ""

@app.route('/health', methods=['GET'])
def health():
    return '{"status":"success"}\n'

@app.route('/v1/ldapsearch', methods=['POST'])
def api():
    data = request.json
    name = request.json["name"]
    conn.search(basedn, '(&(objectclass=inetOrgPerson)(cn=*'+name+'*))', attributes=['uid'])
    res = conn.entries
    num_found = len(res)
    if (num_found == 0):
        return '{"status":"success", "found": '+str(num_found)+'}'
    return '{"status":"success", "found": '+str(num_found)+'}'

if __name__ == '__main__':
    server = ldap3.Server(ldap_host, use_ssl=False, get_info=ldap3.ALL)
    conn = ldap3.Connection(server, binddn, bindpw, auto_bind=True)
    app.run()

